'use strict'
// @flow

import axios from 'axios'
import express from 'express'
import expressBoom from 'express-boom'
import bodyParser from 'body-parser'
import timeout from 'connect-timeout'
import morgan from 'morgan'
import { Cache } from './src/lib/cache'

const port = process.env.PORT || 5790

let app = express()

app.use(morgan('dev'))

app.use(bodyParser.json({limit: '500kb'}))
app.use(expressBoom())
app.use(timeout('5s'))

app.get('/health', (req, res) => res.status(204).send())

// app.get('/api/v0/drones', (req, res) => {
//   axios.get('https://bobs-epic-drone-shack-inc.herokuapp.com/api/v0/drones')
//     .then(apiRes => {
//       console.log(apiRes.status);
//       res.json(apiRes.data);
//     })
//     .catch(err => { 
//       console.log('Error', err);
//       res.json({ message: 'fail'})
//     });
// });

app.get('/api/v0/drones', Cache(() => {
  return axios.get('https://bobs-epic-drone-shack-inc.herokuapp.com/api/v0/drones')
    .then(apiRes => {
      console.log(apiRes.status);
      return apiRes.data
    });
}));

app.all('*', (req, res) => res.status(404).send())

app.use((err, req, res, next) => {
  if (err) {
    res.status(500).send(err.message)
  }
})

app.listen(port, () => console.log('listening on port', port))
