
const db = {}

const Cache = (executer, retryCounter = 0) => (req, res) => {
  executer(req, res)
    .then(apiResponse => {
      console.log('Success')

      db[req.url] = apiResponse
      res.json(apiResponse)
    }, (apiResponseError) => {
      console.log('Error')
      try {
        if (db[req.url]) {
          return res.json(db[req.url])
        }
        // retry
        if (retryCounter < 3) {
          console.log(`Trying for the ${retryCounter} time`)
          return Cache(executer, retryCounter++)(req, res)
        } else {
          console.log('Too many tries')
        }
      } catch (error) {
        console.log({ error })
      }
    })
}

module.exports.Cache = Cache
